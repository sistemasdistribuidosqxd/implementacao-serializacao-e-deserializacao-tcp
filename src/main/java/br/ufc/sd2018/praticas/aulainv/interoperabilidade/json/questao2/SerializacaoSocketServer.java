package br.ufc.sd2018.praticas.aulainv.interoperabilidade.json.questao2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufc.sd2018.praticas.aulainv.interoperabilidade.modelo.Filme;

public class SerializacaoSocketServer {

	public static void main(String[] args) throws Exception {
		try(ServerSocket serverSocket = new ServerSocket(8888);) {

			Socket client = serverSocket.accept();
			DataOutputStream outToClient = new DataOutputStream(client.getOutputStream());
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));

			ObjectMapper mapper = new ObjectMapper();

			String line = inFromClient.readLine();
			System.out.println("########################### dados recebidos ######################");
			System.out.println(line);

			Filme[] filmesRecebidos = mapper.readValue(line, Filme[].class);
			System.out.println("########################### dados convertidos ############################");
			System.out.println(filmesRecebidos);

			Arrays.asList(filmesRecebidos).forEach(f -> f.setTitulo(f.getTitulo() + " altera��o no titulo"));

			System.out.println("########################### altera��o #############################");

			PrintWriter pw = new PrintWriter(outToClient);
			String filmesJSON = mapper.writeValueAsString(Arrays.asList(filmesRecebidos));

			pw.println(filmesJSON);
			pw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
