package br.ufc.sd2018.praticas.aulainv.interoperabilidade.modelo;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class CustomListSerializer extends StdSerializer<List<Filme>> {
	private static final long serialVersionUID = -5633986110699214143L;

	public CustomListSerializer() {
		this(null);
	}

	protected CustomListSerializer(Class<List<Filme>> t) {
		super(t);
	}

	@Override
	public void serialize(List<Filme> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		List<Filme> valueOutput = value.stream()
			.map(f -> new Filme(f.getFilmeID(), 
								f.getTitulo(), 
								f.getAno(), 
								f.getAvaliacaoIMDB(), 
								f.getLinkParaTraillerYoutube()))
			.collect(Collectors.toList());
		gen.writeObject(valueOutput);
	}
}


