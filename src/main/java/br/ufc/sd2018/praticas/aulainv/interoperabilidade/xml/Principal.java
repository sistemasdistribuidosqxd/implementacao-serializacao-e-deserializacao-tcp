package br.ufc.sd2018.praticas.aulainv.interoperabilidade.xml;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import br.ufc.sd2018.praticas.aulainv.interoperabilidade.modelo.Filme;


public class Principal {

	public static void main(String[] args) throws MalformedURLException, JsonProcessingException {
		XmlMapper xmlMapper = new XmlMapper();
		
		Filme f1 = new Filme(1, "titulo 1", 1990, 9, new URL("http://foo.com/1"));
		Filme f2 = new Filme(2, "titulo 2", 1991, 8, new URL("http://foo.com/2"));
		Filme f3 = new Filme(3, "titulo 3", 1992, 7, new URL("http://foo.com/3"));
		Filme f4 = new Filme(4, "titulo 4", 1993, 6, new URL("http://foo.com/4"));

		f1.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f3, f4 }));
		f2.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f1, f3, f4 }));
		f3.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f1, f4 }));
		f4.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f3, f1 }));

		
		String filmesXML = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(Arrays.asList(new Filme[] { f1, f2, f3, f4 }));
		System.out.println(filmesXML);
	}
}
