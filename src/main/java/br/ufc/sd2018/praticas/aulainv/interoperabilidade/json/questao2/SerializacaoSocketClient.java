package br.ufc.sd2018.praticas.aulainv.interoperabilidade.json.questao2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufc.sd2018.praticas.aulainv.interoperabilidade.modelo.Filme;

public class SerializacaoSocketClient {

	public static void main(String[] args) {

		try (Socket clientSocket = new Socket("localhost", 8888);) {
			
			Filme f1 = new Filme(1, "titulo 1", 1990, 9, new URL("http://foo.com/1"));
			Filme f2 = new Filme(2, "titulo 2", 1991, 8, new URL("http://foo.com/2"));
			Filme f3 = new Filme(3, "titulo 3", 1992, 7, new URL("http://foo.com/3"));
			Filme f4 = new Filme(4, "titulo 4", 1993, 6, new URL("http://foo.com/4"));
			
			f1.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f3, f4 }));
			f2.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f1, f3, f4 }));
			f3.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f1, f4 }));
			f4.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f3, f1 }));
			
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			
			ObjectMapper mapper = new ObjectMapper();
			
			String filmesJSON = mapper
					// .writerWithDefaultPrettyPrinter()
					.writeValueAsString(Arrays.asList(new Filme[] { f1, f2, f3, f4 }));
			System.out.println("################### Dados a serem enviados pela rede ####################");
			System.out.println(filmesJSON);
			
			PrintWriter pw = new PrintWriter(outToServer);
			pw.println(filmesJSON);
			pw.flush();
			
			Filme[] filmesRecebidos = mapper.readValue(inFromServer.readLine(), Filme[].class);
			System.out.println("######################### Dados recebidos ############################");
			System.out.println(Arrays.asList(filmesRecebidos));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
