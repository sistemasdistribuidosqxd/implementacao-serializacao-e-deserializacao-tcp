package br.ufc.sd2018.praticas.aulainv.interoperabilidade.json;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufc.sd2018.praticas.aulainv.interoperabilidade.modelo.Filme;

public class Principal {

	/**
	 * Exemplo de serialização e desserialização 
	 */
	public static void main(String[] args) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		Filme f1 = new Filme(1, "titulo 1", 1990, 9, new URL("http://foo.com/1"));
		Filme f2 = new Filme(2, "titulo 2", 1991, 8, new URL("http://foo.com/2"));
		Filme f3 = new Filme(3, "titulo 3", 1992, 7, new URL("http://foo.com/3"));
		Filme f4 = new Filme(4, "titulo 4", 1993, 6, new URL("http://foo.com/4"));

		f1.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f3, f4 }));
		f2.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f1, f3, f4 }));
		f3.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f1, f4 }));
		f4.getTresRelacionados().addAll(Arrays.asList(new Filme[] { f2, f3, f1 }));

		String filmesJSON = objectMapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(Arrays.asList(new Filme[] { f1, f2, f3, f4 }));
		System.out.println(filmesJSON);
		
		List<Filme> listFilmesFromJSON = Arrays.asList(objectMapper.readValue(filmesJSON, Filme[].class));
		System.out.println(listFilmesFromJSON);

		//só pra conferir
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter()
		.writeValueAsString(listFilmesFromJSON));
	}
}
