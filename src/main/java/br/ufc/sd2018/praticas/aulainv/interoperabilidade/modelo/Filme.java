package br.ufc.sd2018.praticas.aulainv.interoperabilidade.modelo;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(Include.NON_NULL)
public class Filme {

	private int filmeID;

	private String titulo;

	private int ano;

	private int avaliacaoIMDB;

	@JsonSerialize(using = CustomListSerializer.class)
	private List<Filme> tresRelacionados;

	private URL linkParaTraillerYoutube;

	public Filme(int filmeID) {
		this.filmeID = filmeID;
	}

	@JsonCreator
	public Filme(@JsonProperty("filmeID") int filmeID, 
				@JsonProperty("titulo") String titulo, 
				@JsonProperty("ano") int ano, 
				@JsonProperty("avaliacaoIMDB") int avaliacaoIMDB, 
				@JsonProperty("linkParaTraillerYoutube") URL linkParaTraillerYoutube) {
		this.filmeID = filmeID;
		this.titulo = titulo;
		this.ano = ano;
		this.avaliacaoIMDB = avaliacaoIMDB;
		this.linkParaTraillerYoutube = linkParaTraillerYoutube;
		this.tresRelacionados = new ArrayList<>();
	}

	public int getFilmeID() {
		return filmeID;
	}

	public void setFilmeID(int filmeID) {
		this.filmeID = filmeID;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getAvaliacaoIMDB() {
		return avaliacaoIMDB;
	}

	public void setAvaliacaoIMDB(int avaliacaoIMDB) {
		this.avaliacaoIMDB = avaliacaoIMDB;
	}

	public List<Filme> getTresRelacionados() {
		return tresRelacionados;
	}

	public void setTresRelacionados(List<Filme> tresRelacionados) {
		this.tresRelacionados = tresRelacionados;
	}

	public URL getLinkParaTraillerYoutube() {
		return linkParaTraillerYoutube;
	}

	public void setLinkParaTraillerYoutube(URL linkParaTraillerYoutube) {
		this.linkParaTraillerYoutube = linkParaTraillerYoutube;
	}

	@Override
	public String toString() {
		return "\nFilme [filmeID=" + filmeID + ", titulo=" + titulo + ", ano=" + ano + ", avaliacaoIMDB=" + avaliacaoIMDB
				+ ", tresRelacionados=" + tresRelacionados + ", linkParaTraillerYoutube=" + linkParaTraillerYoutube
				+ "]";
	}

}
